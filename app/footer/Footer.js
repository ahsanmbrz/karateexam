import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Footer, Tabs, DatePicker, Card, CardItem, Body, List, ListItem, Thumbnail, Text, FooterTab, Badge, Title, Subtitle, Button, Left, Right, Icon, View } from 'native-base';

import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';


export default class ComponentsFooter extends Component {


    goBack() {
        Actions.pop();
    }

    render() {

        return (
                <Footer style={{  backgroundColor: '#5a113d' }}>
                    <FooterTab>
                        <Button  style={{ backgroundColor: '#5a113d' }}  >
                            <Iconss name='swap-vertical' style={{ fontSize: 18, color: 'white' }} />
                            <Text style={{ fontSize: 12, color: 'white' }}>Sort</Text>
                        </Button>
                        <Button  style={{ backgroundColor: '#5a113d'}}>
                            <Iconss name='filter-variant' style={{ fontSize: 18, color: 'white'}} />
                            <Text style={{ fontSize: 12, color: 'white' }}>Filter</Text>
                        </Button>
                        <Button  style={{ backgroundColor: '#5a113d' }}>
                            <Iconss name='map-marker' style={{ fontSize: 18, color: 'white'}} />
                            <Text style={{ fontSize: 12, color: 'white' }}>Maps</Text>

                        </Button>
                    </FooterTab>
                </Footer>
        );
    }
}
