import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Content, Card, CardItem, Body, List, ListItem, Thumbnail, Text, Badge, Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/HeaderTransfer';
export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{ backgroundColor: '#eeeeee' }}>
                    <View style={{ height:500}}>
                        <Card style={{top:15, height: 40, left: 14, width: 330, borderRadius: 10 }}>
                            <CardItem header bordered style={{ height: 40, borderRadius: 10 }}>
                                <Image style={{ width: 61, height: 20 }} source={require('../../assest/images/pembayaran/bni.png')} />
                                <Text style={{ left: 40, color: 'black', fontSize: 18 }}>BNI</Text>
                                <Button transparent onPress={() => navigate('detailpembayaran')} style={{ width: 20, height: 20, left: 196 }}>
                                    <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                                </Button>
                            </CardItem>
                        </Card>

                        <Card style={{top:30, height: 40, left: 14, width: 330, borderRadius: 10 }}>
                            <CardItem header bordered style={{ height: 40, borderRadius: 10 }}>
                                <Image style={{ width: 28, height: 28 }} source={require('../../assest/images/pembayaran/bri.png')} />
                                <Text style={{ left: 73, color: 'black', fontSize: 18 }}>BRI</Text>
                                <Button transparent style={{ width: 12, height: 12, left: 230 }}>
                                    <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                                </Button>
                            </CardItem>
                        </Card>

                        <Card style={{top:45, height: 40, left: 14, width: 330, borderRadius: 10 }}>
                            <CardItem header bordered style={{ height: 40, borderRadius: 10 }}>
                                <Image style={{ width: 50, height: 15 }} source={require('../../assest/images/pembayaran/mandiri.png')} />
                                <Text style={{ left: 51, color: 'black', fontSize: 18 }}>Mandiri</Text>
                                <Button transparent style={{ width: 12, height: 12, left: 175 }}>
                                    <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                                </Button>
                            </CardItem>
                        </Card>

                        <Card style={{top:60, height: 40, left: 14, width: 330, borderRadius: 10 }}>
                            <CardItem header bordered style={{ height: 40, borderRadius: 10 }}>
                                <Image style={{ width: 49, height: 24 }} source={require('../../assest/images/pembayaran/bca.png')} />
                                <Text style={{ left: 52, color: 'black', fontSize: 18 }}>BCA</Text>
                                <Button transparent style={{ width: 12, height: 12, left: 203 }}>
                                    <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                                </Button>
                            </CardItem>
                        </Card>
                    </View>
                </Content>
            </Container>
        );
    }
}