import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Content, Card, CardItem, Body, List, ListItem, Thumbnail, Text, Badge, Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/HeaderPembayaran';
export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{ backgroundColor: '#eeeeee' }}>
                    <Card style ={{height:100 , left : 14, width:330 , borderRadius:10}}>
                        <CardItem header bordered  style={{height:40  , borderRadius:10}}>
                            <Text style={{ color: 'black', fontSize: 14 }}>Transfer</Text>
                            <Button transparent onPress={() => navigate('pembayaransatu')} style={{ width: 20, height: 20, left: 230 }}>
                                <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                            </Button>
                        </CardItem>
                        <CardItem cardBody style={{top :20}}>
                            <Image style={{ left:10,  width:60 , height:20}} source={require('../../assest/images/pembayaran/bni.png')} />
                            <Image style={{ left:30,  width:28 , height:28}} source={require('../../assest/images/pembayaran/bri.png')} />
                            <Image style={{ left:60, width:50 , height:15}} source={require('../../assest/images/pembayaran/mandiri.png')} />
                            <Image style={{ left:90, width:48 , height:24}} source={require('../../assest/images/pembayaran/bca.png')} />
                        </CardItem>
                    </Card>

                    <Card style ={{height:100 , left : 14, width:330  , borderRadius:10}}>
                        <CardItem header bordered  style={{height:40  , borderRadius:10}}>
                            <Text style={{ color: 'black', fontSize: 14 }}>Credit Card</Text>
                            <Button transparent style={{ width: 12, height: 12, left: 213 }}>
                                <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                            </Button>
                        </CardItem>
                        <CardItem cardBody style={{top :20}}>
                            <Image style={{ left:10,  width:37 , height:28}} source={require('../../assest/images/pembayaran/visa.png')} />
                            <Image style={{ left:30,  width:32 , height:24}} source={require('../../assest/images/pembayaran/master_card.png')} />
                          
                        </CardItem>
                    </Card> 

                    <Card style ={{height:100 , left : 14, width:330  , borderRadius:10}}>
                        <CardItem header bordered  style={{height:40  , borderRadius:10}}>
                            <Text style={{ color: 'black', fontSize: 14 }}>ATM</Text>
                            <Button transparent style={{ width: 12, height: 12, left: 254 }}>
                                <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                            </Button>
                        </CardItem>
                        <CardItem cardBody style={{top :20}}>
                            <Image style={{ left:10,  width:32 , height:24}} source={require('../../assest/images/pembayaran/atm.png')} />
                        </CardItem>
                    </Card>

                    <Card style ={{height:100 , left : 14, width:330  , borderRadius:10}}>
                        <CardItem header bordered  style={{height:40  , borderRadius:10}}>
                            <Text style={{ color: 'black', fontSize: 14 }}>Cash</Text>
                            <Button transparent style={{ width: 12, height: 12, left: 251 }}>
                                <Iconsm name='chevron-right' style={{ fontSize: 12 }} ></Iconsm>
                            </Button>
                        </CardItem>
                        <CardItem cardBody style={{top :20}}>
                          
                        </CardItem>
                    </Card>

                </Content>


            </Container>
        );
    }
}