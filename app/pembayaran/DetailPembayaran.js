import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Footer, FooterTab, Content, Item, Card, CardItem, Input, Text, Button, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/HeaderDetail';
export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{ backgroundColor: '#eeeeee' }}>
                    <View style={{ height: 620 }}>
                        <View style={{ top: 15, left: 27, width: 280 }}>
                            <Text style={{ color: '#808080', fontSize: 14, fontStyle: 'italic' }}>Hai, Rizal Ahmad Hasan mohon teliti kembali sebelum melakukan pembayaran atas nama :</Text>
                        </View>
                        <Card style={{ top: 30, height: 50 }}>
                            <CardItem header bordered >
                                <Iconsm name='account-box' style={{ fontSize: 18, top: 0 }}></Iconsm>
                                <Text style={{ fontSize: 14, color: 'black', top: -5, left: 19 }}>Rizal Ahmad Hasan</Text>
                                <Text note numberOfLines={2} style={{ fontSize: 8, color: 'black', left: -103, top: 10 }}>LKL3200001</Text>
                            </CardItem>
                            <View>
                                <Text style={{ fontSize: 12, color: '#808080', left: 260, top: -35 }}>Rp 1.500.000</Text>
                            </View>
                        </Card>

                        <Card style={{ top: 30, height: 50 }}>
                            <CardItem header bordered >
                                <Iconsm name='account-box' style={{ fontSize: 18, top: 0 }}></Iconsm>
                                <Text style={{ fontSize: 14, color: 'black', top: -5, left: 19 }}>Ramdhanni</Text>
                                <Text note numberOfLines={2} style={{ fontSize: 8, color: 'black', left: -52, top: 10 }}>LKL3200002</Text>
                            </CardItem>
                            <View>
                                <Text style={{ fontSize: 12, color: '#808080', left: 260, top: -35 }}>Rp 1.500.000</Text>
                            </View>
                        </Card>

                        <View style={{ left: 14, top: 50 }}>
                            <Text style={{ color: 'black', fontSize: 14 }}>Total Pembayaran</Text>
                            <Text style={{top:8, color: '#808080', fontSize: 12, fontStyle: 'italic' }}>Rp 3.000.000</Text>
                            <Text style={{ top: 20, color: 'black', fontSize: 14, fontStyle: 'italic' }}>Metode Pembayaran</Text>
                            <Image style={{top:25 , width: 49, height: 16 }} source={require('../../assest/images/pembayaran/bni.png')} />
                            <Text style={{ top: 40, color: 'black', fontSize: 14, fontStyle: 'italic' }}>Jenis Event</Text>
                            <Text style={{top: 45, color: '#808080', fontSize: 12, fontStyle: 'italic' }}>Ujian DAN Zona Jabar 2019</Text>
                            <Text style={{ top: 60, color: 'black', fontSize: 14, fontStyle: 'italic' }}>Kode Pembayaran</Text>
                            <Item regular style={{top:65, borderColor:'#00a1f1' , width:120, height:24}}>
                                <Input >
                                    <Text style={{fontSize:14, color:'#00a1f1'}}>TL0-898-864</Text>
                                </Input>
                            </Item>
                            <TouchableOpacity style={{top:87}} >
                                <Text style={{fontSize:14, textDecorationLine:'underline', fontStyle:'italic', color:'#00a1f1'}}>Detail</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                </Content>
                <Footer style={{ backgroundColor: '#e67e22' }}>
                    <FooterTab>
                        <Button onPress={() => navigate('infopembayaran')} style={{ backgroundColor: '#e67e22' }}  >
                            <Text style={{ fontSize: 16, color: 'white' }}>LANJUTKAN</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}