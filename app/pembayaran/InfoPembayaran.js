import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Footer, FooterTab, Content, Item, Card, Icon, CardItem, Input, Text, Button, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/HeaderInfoPembayaran';
export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{ backgroundColor: 'white' }}>
                    <View style={{ height: 620 }}>
                        <View style={{ backgroundColor: '#bababa', width: 360, height: 50, flexDirection: 'row' }}>
                            <Image style={{ top: 17, left: 14, width: 18, height: 18 }} source={require('../../assest/images/pembayaran/info/one.png')} />
                            <Text style={{ top: 14, left: 45, color: 'black', fontSize: 16 }}>Selesaikan Pembayaran Sebelum</Text>
                        </View>
                        <View style={{ backgroundColor: 'white', width: 360, height: 70, flexDirection: 'row' }}>
                            <Iconsm name='access-time' style={{ top: 17, left: 14, fontSize: 20 }} ></Iconsm>
                            <Text style={{ top: 14, left: 45, color: 'black', fontSize: 14 }}>(Jum'at, 26 April 2019, 12:30)  </Text>
                        </View>
                        <View style={{ backgroundColor: '#bababa', width: 360, height: 50, flexDirection: 'row' }}>
                            <Image style={{ top: 17, left: 14, width: 18, height: 18 }} source={require('../../assest/images/pembayaran/info/two.png')} />
                            <Text style={{ top: 14, left: 45, color: 'black', fontSize: 16 }}>Mohon Transfer ke</Text>
                        </View>
                        <View style={{ backgroundColor: 'white', width: 360, height: 70 }}>
                            <Iconsm name='person-outline' style={{ top: 17, left: 14, fontSize: 20 }} ></Iconsm>
                            <Text note numberOfLines={0} style={{ top: -10, left: 63, color: 'black', fontSize: 14 }}>Nomor Rekening </Text>
                            <Text note numberOfLines={1} style={{ top: -10, left: 63, color: 'black', fontSize: 14 }}>8341 2138 23 </Text>
                            <TouchableOpacity style={{ left: 300, top: -35 }}>
                                <Text style={{ fontSize: 14, color: '#00a1f1' }}>SALIN</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ backgroundColor: '#d1d6d8', width: 360, height: 60 }}>
                            <Text note numberOfLines={0} style={{top:10, left: 63, color: 'black', fontSize: 14 }}>Nama Pemilik </Text>
                            <Text note numberOfLines={1} style={{top:10,  left: 63, color: 'black', fontSize: 14 }}>PT.Brilyan Trimatra Utama</Text>
                            <Image style={{ top: -15, left: 300, width: 30, height: 10 }} source={require('../../assest/images/pembayaran/bni.png')} />
                        </View>
                        <View style={{ backgroundColor: 'white', width: 360, height: 70 }}>
                            <Text note numberOfLines={0} style={{top:15,  left: 63, color: 'black', fontSize: 14 }}>Jumlah Total </Text>
                            <Text note numberOfLines={1} style={{top:15,  left: 63, color: 'black', fontSize: 14 }}>Rp 3.000.000</Text>
                            <TouchableOpacity style={{ left: 300, top: -25 }}>
                                <Text style={{ fontSize: 14, color: '#00a1f1' }}>SALIN</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ backgroundColor: '#bababa', width: 360, height: 50, flexDirection: 'row' }}>
                            <Image style={{ top: 17, left: 14, width: 18, height: 18 }} source={require('../../assest/images/pembayaran/info/three.png')} />
                            <Text style={{ top: 14, left: 45, color: 'black', fontSize: 16 }}>Anda Sudah Membayar?</Text>
                        </View>
                        <View style={{ backgroundColor: 'white', width: 320, height: 50 }}>
                            <Text style={{ top: 14, left: 22, color: 'black', fontSize: 14, textAlign:'center' }}>Setelah pembayaran Anda dikonfirmasi, kami akan mengirim bukti pendaftaran</Text>
                        </View>
                    </View>
                </Content>
                <View style={{ justifyContent: 'center', alignItems: 'center', left: 20, width: 320, bottom: 14 }}>
                    <Button onPress={() => navigate('infopembayaran')} style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#eeeeee', width: 320 }}  >
                        <Text style={{ fontSize: 16, color: '#00a1f1' }}>Saya Sudah Bayar</Text>
                    </Button>
                </View>
            </Container>
        );
    }
}