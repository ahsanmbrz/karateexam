import React, { Component } from 'react';
import  { StyleSheet , Image ,TouchableOpacity,DatePicker, TextInput} from 'react-native';
import { Container, Header, Content, Footer,Card,CardItem, Body, List, ListItem, Thumbnail, Text, FooterTab, Badge,  Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import {Actions} from 'react-native-router-flux';


export default class ComponentsHeader extends Component {

  goBack() {
      Actions.pop();
  }

  render() {
    return (
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
            <TouchableOpacity onPress={this.goBack}>
            <Iconsm name='arrow-back' style={{ fontSize: 20,color: 'white' , alignItems:'center', justifyContent:'center'}}/>
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>Pendaftaran</Title>
          </Body>
        </Header>
    );
  }
}

const styles = StyleSheet.create({

    button: {
      width:328,
      height:39,
      backgroundColor:'#0091ea',
       borderRadius: 6,
        marginVertical: 10,
        paddingVertical: 10
    },
    buttonText: {
      fontSize:16,
      fontFamily: 'sans-serif',
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    }
  
  });
