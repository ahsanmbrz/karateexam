import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

import Footer from '../footer/Footer';
import Header from '../header/Header';
import Home from '../home/Home';
import Daftar from '../daftar/Daftar';
import Pendaftaran from '../pendaftaran/Pendaftaran';
import TambahPendaftaran from '../pendaftaran/TambahPendaftaran';
import Pembayaran from '../pembayaran/Pembayaran';
import Pembayaransatu from '../pembayaran/PembayaranSatu';
import DetailPembayaran from '../pembayaran/DetailPembayaran';
import InfoPembayaran from '../pembayaran/InfoPembayaran';




export default class Routes extends Component<{}> {
	render() {
		return(
			<Router>
			    <Stack key="root" hideNavBar={true}>
			      <Scene key="home" component={Home} title="home" initial={true}/>
                  <Scene key="daftar" component={Daftar} title="daftar"/>
                  <Scene key="pendaftaran" component={Pendaftaran} title="pendaftaran"/>
				  <Scene key="tpendaftaran" component={TambahPendaftaran} title="pendaftaran"/>
				  <Scene key="pembayaran" component={Pembayaran} title="pembayaran"/>
				  <Scene key="pembayaransatu" component={Pembayaransatu} title="pembayaran"/>
				  <Scene key="detailpembayaran" component={DetailPembayaran} title="pembayaran"/>
				  <Scene key="infopembayaran" component={InfoPembayaran} title="pembayaran"/>
			    </Stack>
			 </Router>
			)
	}
}
