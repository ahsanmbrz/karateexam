import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput, PermissionsAndroid, Alert, ActivityIndicator, Dimensions } from 'react-native';
import { Container, Content, Footer, FooterTab, Card, CardItem, Body, List, ListItem, Thumbnail, Text, Badge, Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Marker } from 'react-native-maps';
const { width, height } = Dimensions.get('window');

export default class ComponentsHome extends Component {

    constructor(props) {
        super(props);


        this.state = {
            region: {
                latitude: "",
                longitude: "",
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
                accuracy: ""
            },
            listdojo: [
                {
                    latlong: {
                        latitude: -6.285224,
                        longitude: 106.9785166
                    },
                    name: "Aula Ibrahim Baitul Jihad",
                    alamat: "Jl. Kemang Dahlia Raya Blok ZZ, Kemang Pratama 2 No.01, Bojong Rawalumbu, Kec. Rawalumbu, Kota Bks, Jawa Barat 17116",
                    image: require("../image/ski.jpg")
                },
            ],
            bottom: 1
        };
    }
    goBack() {
        Actions.pop();
    }
    componentDidMount() {
        this.permissioncheck();
    }
    permissioncheck() {
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                title: "Location Accessing Permission",
                message: "App needs access to your location"
            }
        );
        this.getcurrentlocation();
    }
    getcurrentlocation() {
        Geolocation.getCurrentPosition(
            (position) => {

                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        accuracy: position.coords.accuracy,
                        latitudeDelta: 0.1322,
                        longitudeDelta: 0.0074
                    }
                });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }
    marker() {
        return {
            latitude: this.state.region.latitude,
            longitude: this.state.region.longitude
        }
    }
    toolbarHack = () => {
        if (this.state.bottom === 1) {
            this.setState({
                bottom: 0
            })
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>

                <View style={{ position: 'absolute', left:'2%',top: '2%', justifyContent: 'center' }}>
                    <Button style={{ backgroundColor: 'white', flex: 1, width: 26, height: 26, borderRadius: 50, borderWidth: 2, borderColor: 'white', alignItems: 'center', justifyContent: 'center' }} >
                        <Iconsm name='arrow-back' style={{ fontSize: 16, color: 'black', alignItems: 'center', justifyContent: 'center' }} />
                    </Button>
                </View>
                <View style={{ height: '35%' }}>

                    {
                        this.state.region.latitude ?
                            <MapView
                                toolbarEnabled={true}
                                initialRegion={this.state.region}
                                style={{ flex: 1, width: width, bottom: this.state.bottom }}

                            >
                                <MapView.Marker
                                    coordinate={this.marker()}
                                    title="You"
                                    description="You are here!"
                                    pinColor='blue'
                                />
                                {this.state.listdojo.map((item, key) => (
                                    <MapView.Marker
                                        coordinate={item.latlong}
                                        title={item.name}
                                        description={item.alamat}
                                        pinColor='red'
                                        onPress={() => this.toolbarHack()}

                                    />
                                )
                                )}
                            </MapView>
                            : <ActivityIndicator size='large' color="#0000ff" />
                    }
                </View>

                <View style={{ flexDirection: 'row', height: 40 }}>
                    <Button vertical style={{ flex: 1, backgroundColor: '#41062a', justifyContent: 'center' }}>
                        <Text>GASHUKU</Text>

                    </Button>
                    <Button vertical style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center' }}>
                        <Text style={{ color: 'black' }}>UJIAN</Text>
                    </Button>
                </View>

                <Content style={{ height: '100%', backgroundColor: '#eeeeee' }}>
                    <View style={{ left: 14, width: 300 }}>
                        <Text style={{ fontSize: 20 }}>"GASHUKU" UJIAN DAN ZONA 2 JABAR -2019</Text>
                    </View>

                    <View style={{ left: 14, width: 300, height: 330, top: 10 }}>
                        <Iconsm name='location-city' style={{ fontSize: 16 }}></Iconsm>
                        <Text style={{ top: -20, left: 20, fontSize: 17 }}>HALL BASKET KOTA BEKASI </Text>
                        <Text note numberOfLines={1} style={{ top: -20, left: 20, fontSize: 12 }}>Jum'at- Sabtu, 26 - 27 April 2019 </Text>
                        <Text note numberOfLines={2} style={{ top: -20, left: 20, fontSize: 12 }}>08.00 s/d Selesai </Text>

                        <Iconsm name='assignment' style={{ fontSize: 16 }}></Iconsm>
                        <Text style={{ top: -20, left: 20, fontSize: 17 }}>REGISTRASI </Text>
                        <Text note numberOfLines={1} style={{ top: -20, left: 20, fontSize: 12 }}>1 - 26 APRIL 2019</Text>

                        <Iconsm name='verified-user' style={{ fontSize: 16 }}></Iconsm>
                        <Text style={{ top: -20, left: 20, fontSize: 17 }}>VERIFIKASI </Text>
                        <Text note numberOfLines={1} style={{ top: -20, left: 20, fontSize: 12 }}>Kamis, 25 April 2019</Text>
                        <Text note numberOfLines={2} style={{ top: -20, left: 20, fontSize: 12 }}>14.00 - selesai</Text>

                        <Image source={require('../../assest/images/daftar/contact.png')} style={{ width: 14, height: 16 }}></Image>
                        <Text style={{ top: -20, left: 20, fontSize: 17 }}>KONTAK PERSON </Text>
                        <Text note numberOfLines={1} style={{ top: -20, left: 20, fontSize: 12 }}>08995118887</Text>
                    </View>
                </Content>
                <View style={{ position: 'absolute', bottom: '10%', alignSelf: 'center', justifyContent: 'center' }}>
                    <Button style={{ backgroundColor: '#4a0931', flex: 1, width: 40, height: 40, borderRadius: 50, borderWidth: 2, borderColor: '#2a001a', alignItems: 'center', justifyContent: 'center' }} >
                        <Icons name='chevron-up' style={{ fontSize: 12, color: 'white', alignItems: 'center', justifyContent: 'center' }} />
                    </Button>
                </View>
                <Footer style={{ backgroundColor: '#e67e22' }}>
                    <FooterTab>
                        <Button onPress={() => navigate('pendaftaran')} style={{ backgroundColor: '#e67e22' }}  >
                            <Text style={{ fontSize: 16, color: 'white' }}>DAFTAR</Text>
                        </Button>
                    </FooterTab>
                </Footer>

            </Container>
        );
    }
}