**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet } from 'react-native';
import { Image , TouchableOpacity,PermissionsAndroid,Alert,ActivityIndicator, Dimensions} from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Marker } from 'react-native-maps';
import { Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View,  } from "native-base";

const {width,height} = Dimensions.get('window');

export default class CardItemBordered extends Component {
  constructor(props) {
    super(props);

  
    this.state = {
      region: {
                latitude: "",
                longitude: "",
                latitudeDelta:0.0922,
                longitudeDelta:0.0421,
                accuracy:""},
                 listdojo:[
                        {latlong:{latitude:-6.260809,
                        longitude:106.964638},
                        name:"Dojo Intelektika",
                        alamat:"Aula Blabla",
                        image:require("../image/ski.jpg")},
                         {latlong:{latitude:-6.280809,
                        longitude:106.974638},
                        name:"Dojo Garuda Muda",
                        alamat:"Bekasi blabla",
                        image:require("../image/kejurnas.jpg")},
                         {latlong:{latitude:-6.200809,
                        longitude:106.994638},
                        name:"Dojo ketiga",
                        alamat:"Dekat Blabla",
                        image:require("../image/f2.png")},
                        ],
                        bottom: 1 
              };
  }
  goBack() {
    Actions.pop();
  }
  componentDidMount(){
    this.permissioncheck();
  }
  permissioncheck(){
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Location Accessing Permission",
        message: "App needs access to your location"
      }
    );
    this.getcurrentlocation();
  }
  getcurrentlocation(){
        Geolocation.getCurrentPosition(
            (position) => {

                this.setState({
                   region:{
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                    accuracy:position.coords.accuracy,
                    latitudeDelta:0.1322,
                    longitudeDelta:0.0074}                    
                });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
  } 
   marker(){
      return {
          latitude:this.state.region.latitude,
          longitude:this.state.region.longitude
      }
  }
  toolbarHack = () => {
          if(this.state.bottom === 1){
            this.setState({
              bottom: 0
            })
          } 
        }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
          <TouchableOpacity onPress={this.goBack}>
              <Icon name='arrow-back' style={{fontSize: 30, color: 'white'}} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>K-DOJO</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='search' />
            </Button>
          </Right> 
        </Header>

       
           {
                this.state.region.latitude ? 
                <MapView 
                 toolbarEnabled={true}
                 initialRegion={this.state.region}
                 style={{flex: 1 , width:width,bottom:this.state.bottom}}

                  >
                 <MapView.Marker
                    coordinate={this.marker()}
                    title="You"
                    description="You are here!"
                    pinColor='red'
                 />
                 { this.state.listdojo.map((item, key)=>(
                   <MapView.Marker
                    coordinate={item.latlong}
                    title={item.name}
                    description={item.alamat}
                    pinColor='blue'
                    onPress={() => this.toolbarHack()}

                 />
                  )
                )}
                </MapView>
                : <ActivityIndicator size="large" color="#0000ff" /> 
            }
      

        <Content>
         { 
          this.state.listdojo.map(function(item, key){
            return(
           <List >
            <ListItem thumbnail onPress={() => navigate('profil')}>
              <Left>
              <View>
                <Image source={item.image} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>{item.name}</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>{item.alamat}</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
          )
        })}
        </Content>
         {/* <List>
            <ListItem thumbnail>
              <Left>
              <View style>
                <Image source={ require('../image/ski.jpg')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text onPress={() => navigate('profil')}>Dojo Intelektika</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat </Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>*/}
          {/*<List>
            <ListItem thumbnail>
              <Left>
                <View style>
                <Image source={ require('../image/kejurnas.jpg')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>Dojo Garuda Muda</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
          <List>
            <ListItem thumbnail>
              <Left>
              <View style>
                <Image source={ require('../image/f2.png')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>Dojo Karasu</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
           <List>
            <ListItem thumbnail>
              <Left>
              <View style>
                <Image source={ require('../image/pnup.png')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>Dojo Macan Putih</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
           <List>
            <ListItem thumbnail>
              <Left>
              <View style>
                <Image source={ require('../image/foto1.jpg')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>Dojo Baitul Jihad</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
           <List>
            <ListItem thumbnail>
              <Left>
              <View style>
                <Image source={ require('../image/tes.jpg')} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>Dojo Karasu</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>: Aula Ibrahim jl. Dahlia Raya Kemang Pratama 2,Kota Bekasi Jawa Barat</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>*/}
        
         <Footer style={{backgroundColor:'#4a0931'}}>
          <FooterTab style={{backgroundColor:'#4a0931'}}>
            <Button vertical>
              <Icons name="sort" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Sort</Text>
            </Button>
            <Button vertical active>
              <Icons active name="filter" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Filter</Text>
            </Button>
            <Button vertical>
              <Icons name="map-marker" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Maps</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}



<Button onPress={this.goBack} style={{ width: 26, height: 26, backgroundColor: 'white', justifyContent: 'center', borderRadius: 20, top: -165, left: 12 }}>
<Iconsm name='arrow-back' style={{ fontSize: 16 }}></Iconsm>
</Button>
<Button style={{ width: 26, height: 26, backgroundColor: 'white', justifyContent: 'center', borderRadius: 20, top: '-50%', left: '180%' }}>
<Iconsm name='directions' style={{ fontSize: 16 }}></Iconsm>
</Button>
