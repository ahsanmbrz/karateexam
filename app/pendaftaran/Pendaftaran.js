import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Content, Card, CardItem, Body, List, ListItem, Thumbnail, Text, Badge, Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/HeaderPendaftaran';


export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{ backgroundColor: '#eeeeee' }}>
                    <Card style={{ left: 14, width: 328, top: 10, height: 104.5 }}>
                        <CardItem header style={{ backgroundColor: '#4d2d86' }}>
                            <Text style={{ fontSize: 24, color: 'white', top: -5 }}>Ujian Kota Bekasi</Text>
                        </CardItem>
                        <CardItem style={{ backgroundColor: '#9979D2', top: -20 }}>
                            <Iconsm name='event-note' style={{ fontSize: 16, top: -8 }}></Iconsm>
                            <Iconsm name='location-city' style={{ top: 15, left: -15, fontSize: 16 }}></Iconsm>
                            <Body>
                                <Text style={{ fontSize: 12 }}>
                                    Jum'at 26 April 2019
                                </Text>
                                <Text note numberOfLines={1} style={{ top: 5, color: 'black', fontSize: 12 }}>
                                    HALL BASKET KOTA BEKASI
                                </Text>
                            </Body>

                        </CardItem>
                    </Card>
                    <Card style={{ borderRadius: 10, borderWidth: 1, left: 14, width: 328, top: 10, height: 57 }}>
                        <CardItem header style={{ borderRadius: 10, borderWidth: 1  }}>
                            <Iconsm name='account-box' style={{ fontSize: 18, top: 0 }}></Iconsm>
                            <Text style={{ fontSize: 14, color: 'black', top: -5, left: 19 }}>Rizal Ahmad Hasan</Text>
                            <Text ote numberOfLines={2} style={{ fontSize: 8, color: 'black', left: -103, top: 10 }}>LKL3200001</Text>
                            <View>
                                <Button transparent style ={{width: 20, height:20 , left: 85 }}>
                                    <Iconsm name='remove-circle-outline' style={{ fontSize: 20, top: 0 }}></Iconsm>
                                </Button>
                            </View>
                        </CardItem>
                    </Card>
                    <Card style={{ borderRadius: 20, borderWidth: 1, left: 14, width: 328, top: 10, height: 61.5 }}>
                        <CardItem header style={{ borderRadius: 10, borderWidth: 1  }}>
                            <Iconsm name='account-box' style={{ fontSize: 18, top: 0, color:'#757575' }}></Iconsm>
                            <Text style={{ fontSize: 14, color: '#757575', top: 0, left: 19 }}>Tambah Peserta</Text>
                            {/*<Text ote numberOfLines={2} style={{ fontSize: 8, color: '#757575', left: -103, top: 10 }}></Text>*/}
                            <View>
                                <Button transparent onPress={() => navigate('tpendaftaran')} style ={{ left: 150, width: 20, height:20  }} >
                                    <Iconsm name='add-circle-outline'  style={{ fontSize: 20, top: 0, color:'#757575' }}></Iconsm>
                                </Button>
                            </View>
                        </CardItem>
                    </Card>
                </Content>

                <View style={{ backgroundColor: 'white', height: 76, left: 14, top: 10 }}>
                    <Text style={{ fontStyle: 'italic', fontSize: 10, color: '#808080' }}>Total Pembayaran 2 orang</Text>
                    <Text style={{ fontStyle: 'normal', fontSize: 14, color: 'black' }}>Rp.150.000</Text>
                    <Text style={{ fontStyle: 'italic', fontSize: 10, color: '#808080' , top:5}}>1 x Rp.150.000 Rincian</Text>
                    <View style={{ left: 246, top: -40 }}>
                        <Button transparent onPress={() => navigate('pembayaran')} style={{ width: 93, height: 36, borderRadius: 10, backgroundColor: '#e67e22', justifyContent: 'center' }}>
                            <Text style={{color:'white'}}>Lanjut</Text>
                        </Button>
                    </View>
                </View>


            </Container>
        );
    }
}