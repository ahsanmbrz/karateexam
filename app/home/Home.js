import React, { Component } from 'react';
import { StyleSheet, Image, TouchableOpacity, DatePicker, TextInput } from 'react-native';
import { Container, Content, Card, CardItem, Body, List, ListItem, Thumbnail, Text, Badge, Title, Button, Left, Right, Icon, View } from 'native-base';

import Icons from 'react-native-vector-icons/FontAwesome5';
import Iconss from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconsf from 'react-native-vector-icons/AntDesign';
import Iconsm from 'react-native-vector-icons/MaterialIcons';
import { Actions } from 'react-native-router-flux';
import Header from '../header/Header';
import Footer from '../footer/Footer';

export default class ComponentsHome extends Component {

    goBack() {
        Actions.pop();
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <Header />
                <Content style={{backgroundColor:'#eeeeee'}}>
                    <Card>
                        <CardItem header>
                            <Text>UJIAN DAN ZONA 2 JABAR -2019</Text>
                        </CardItem>
                        <CardItem style={{ top: -20 }}>
                            <Image source={require('../../assest/images/home/map.png')} style={{ top: -23, width: 8, height: 12 }}></Image>
                            <Image source={require('../../assest/images/home/date.png')} style={{ top: 12, left: -8, width: 10, height: 10 }}></Image>
                            <Image source={require('../../assest/images/home/time.png')} style={{ left: -18, top: 35, width: 10, height: 10 }}></Image>
                            <Body>
                                <Text style={{ fontSize: 12 }}>
                                    Aula Ibrahim,Jl Dahlia Raya, Kemang Pratama 2 Kota Bekasi
                                </Text>

                                <Text note numberOfLines={1} style={{ top: 5, color: 'black', fontSize: 12 }}>
                                    Jumat - Minggu 26 - 28 April 2019
                                </Text>
                                <Text note numberOfLines={2} style={{ top: 10, color: 'black', fontSize: 12 }}>
                                    08.00 s/d Selesai
                                </Text>
                            </Body>

                            <Button onPress={() => navigate('daftar')} style={{ top: 40, width: 60, height: 16, borderRadius: 10, backgroundColor: '#00a1f1' }}>
                                <Text style={{ fontSize: 10 }}>Open</Text>
                            </Button>

                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem header>
                            <Text>UJIAN KYU KOTA BEKASI Semester I - 2019</Text>
                        </CardItem>
                        <CardItem style={{ top: -20 }}>
                            <Image source={require('../../assest/images/home/map.png')} style={{ top: -23, width: 8, height: 12 }}></Image>
                            <Image source={require('../../assest/images/home/date.png')} style={{ top: 12, left: -8, width: 10, height: 10 }}></Image>
                            <Image source={require('../../assest/images/home/time.png')} style={{ left: -18, top: 35, width: 10, height: 10 }}></Image>
                            <Body>
                                <Text style={{ fontSize: 12 }}>
                                    Aula Ibrahim,Jl Dahlia Raya, Kemang Pratama 2 Kota Bekasi
                                </Text>

                                <Text note numberOfLines={1} style={{ top: 5, color: 'black', fontSize: 12 }}>
                                    Jumat - Minggu 26 - 28 April 2019
                                </Text>
                                <Text note numberOfLines={2} style={{ top: 10, color: 'black', fontSize: 12 }}>
                                    08.00 s/d Selesai
                                </Text>
                            </Body>

                            <Button style={{ top: 40, width: 60, height: 16, borderRadius: 10, backgroundColor: '#c0392b' }}>
                                <Text style={{ fontSize: 9}}>Close</Text>
                            </Button>

                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem header>
                            <Text>UJIAN KYU KOTA BEKASI Semester I - 2018</Text>
                        </CardItem>
                        <CardItem style={{ top: -20 }}>
                            <Image source={require('../../assest/images/home/map.png')} style={{ top: -23, width: 8, height: 12 }}></Image>
                            <Image source={require('../../assest/images/home/date.png')} style={{ top: 12, left: -8, width: 10, height: 10 }}></Image>
                            <Image source={require('../../assest/images/home/time.png')} style={{ left: -18, top: 35, width: 10, height: 10 }}></Image>
                            <Body>
                                <Text style={{ fontSize: 12 }}>
                                    Aula Ibrahim,Jl Dahlia Raya, Kemang Pratama 2 Kota Bekasi
                                </Text>

                                <Text note numberOfLines={1} style={{ top: 5, color: 'black', fontSize: 12 }}>
                                    Jumat - Minggu 26 - 28 April 2019
                                </Text>
                                <Text note numberOfLines={2} style={{ top: 10, color: 'black', fontSize: 12 }}>
                                    08.00 s/d Selesai
                                </Text>
                            </Body>

                            <Button style={{ top: 40, width: 60, height: 16, borderRadius: 10, backgroundColor: '#c0392b' }}>
                                <Text style={{ fontSize: 9}}>Close</Text>
                            </Button>

                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem header>
                            <Text>UJIAN KYU KOTA BEKASI Semester I - 2017</Text>
                        </CardItem>
                        <CardItem style={{ top: -20 }}>
                            <Image source={require('../../assest/images/home/map.png')} style={{ top: -23, width: 8, height: 12 }}></Image>
                            <Image source={require('../../assest/images/home/date.png')} style={{ top: 12, left: -8, width: 10, height: 10 }}></Image>
                            <Image source={require('../../assest/images/home/time.png')} style={{ left: -18, top: 35, width: 10, height: 10 }}></Image>
                            <Body>
                                <Text style={{ fontSize: 12 }}>
                                    Aula Ibrahim,Jl Dahlia Raya, Kemang Pratama 2 Kota Bekasi
                                </Text>

                                <Text note numberOfLines={1} style={{ top: 5, color: 'black', fontSize: 12 }}>
                                    Jumat - Minggu 26 - 28 April 2019
                                </Text>
                                <Text note numberOfLines={2} style={{ top: 10, color: 'black', fontSize: 12 }}>
                                    08.00 s/d Selesai
                                </Text>
                            </Body>

                            <Button style={{ top: 40, width: 60, height: 16, borderRadius: 10, backgroundColor: '#00a1f1' }}>
                                <Text style={{ fontSize: 10}}>Open</Text>
                            </Button>

                        </CardItem>
                    </Card>

                </Content>
                <Footer />
            </Container>
        );
    }
}